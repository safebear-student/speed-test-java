$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:toolslist.features/login.feature");
formatter.feature({
  "name": "Login",
  "description": "  Rules:\n  1. The user must be warned if a login fails\n  2. The user must be told if a login is successful",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Navigate and login to the application",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HighImpact"
    },
    {
      "name": "@HighRisk"
    }
  ]
});
formatter.step({
  "name": "I navigate to the login page",
  "keyword": "Given "
});
formatter.step({
  "name": "I enter the login details for a \u0027\u003cuserType\u003e\u0027",
  "keyword": "When "
});
formatter.step({
  "name": "I can see the following message: \u0027\u003cvalidationMessage\u003e\u0027",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "userType",
        "validationMessage"
      ]
    },
    {
      "cells": [
        "invalidUser",
        "Username or Password is incorrect"
      ]
    },
    {
      "cells": [
        "validUser",
        "Login Successful"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Navigate and login to the application",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HighImpact"
    },
    {
      "name": "@HighRisk"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs.i_navigate_to_the_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the login details for a \u0027invalidUser\u0027",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs.i_enter_the_login_details_for_a(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see the following message: \u0027Username or Password is incorrect\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.i_can_see_the_following_message(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Navigate and login to the application",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HighImpact"
    },
    {
      "name": "@HighRisk"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefs.i_navigate_to_the_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the login details for a \u0027validUser\u0027",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefs.i_enter_the_login_details_for_a(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see the following message: \u0027Login Successful\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs.i_can_see_the_following_message(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});