package com.safebear.auto.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;


/**
 * This is our configuration file for Selenium (WebDriver)
 *
 * It sets the URL of the test environment
 * It sets what browser we're using
 */
public class Utils {

    /**
     * private = only for this class
     * static = only one variable
     * final = can't be changed after its value is set
     *
     * Name of the variable is in caps because it never changes
     *
     * System.getProperty looks at our CI tool and gets the URL for the test environment
     * The default test environment is the 'def' value
     */
    private static final String URL =  System.getProperty("url", "http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    /**
     * Getter for the URL
     *
     * @return the URL of my test environment / application
     */
    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){

        // This tells selenium where our chromedriver is
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        // This tells selenium where our firefox driver is
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        // This tells selenium where our ie driver is
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer314149.exe");

        // Here we're setting the size of our browser
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,778", "incognito");

        // Here we're returning the driver for our chosen browser
        switch (BROWSER) {

            case "chrome":

                return new ChromeDriver(options);

            case "headless":

                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            case "firefox":

                return new FirefoxDriver();

            case "ie":
                return new InternetExplorerDriver();

            default:
                return new ChromeDriver(options);

        }


    }

    public static String generateScreenShotFileName(){
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date() + ".png");
    }

    public static void capturescreenshot(WebDriver driver, String fileName){

        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        File file = new File("target/screenshots");

        if(!file.exists()){
            if(file.mkdir()){
                System.out.println("Directory is created");
            } else {
                System.out.println("Failed to create directory");
            }
        }

        try {
            copy(srcFile, new File("target/screenshots/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static WebElement waitForElement(By locator, WebDriver driver){

        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        try{
            element = driver.findElement(locator);
        }catch (TimeoutException e){
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            element = driver.findElement(locator);
        } catch (Exception e){
            System.out.println(e.toString());
            capturescreenshot(driver, generateScreenShotFileName());
            throw (e);
        }

        return element;
    }


}

