package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

@CucumberOptions(

        // This is where my html report will be (in the target/cucumber directory)
        plugin = {"pretty", "html:target/cucumber"},

        // This is changing to 'not @to-do' in future cucumber releases
        tags = "~@to-do",

        // This is where our tests will be created
        glue = "com.safebear.auto.tests",

        // This is running all the features in this folder
        features = "classpath:toolslist.features"
)
public class RunCukes extends AbstractTestNGCucumberTests {

    @AfterClass
    public static void closeBrowser(){
        // This quits the browser after each scenario
        StepDefs.driver.quit();
    }

}
