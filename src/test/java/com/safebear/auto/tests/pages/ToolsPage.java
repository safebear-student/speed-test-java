package com.safebear.auto.tests.pages;


import com.safebear.auto.tests.pages.locators.ToolsPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    // ACTIONS


    // CHECKS

    public String checkLoginSuccessfulMessage(){
        return Utils.waitForElement(locators.getSuccessMessageLocator(), driver).getText();
    }

}
