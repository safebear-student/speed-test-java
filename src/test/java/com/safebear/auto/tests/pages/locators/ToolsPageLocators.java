package com.safebear.auto.tests.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    // Message
    private By successMessageLocator = By.xpath("//b[.=\"Login Successful\"]");

}
